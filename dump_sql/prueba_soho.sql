-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 23-02-2021 a las 01:36:06
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.21

START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba_soho`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(100) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_02_21_151008_create_proyects_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(100) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyects`
--

DROP TABLE IF EXISTS `proyects`;
CREATE TABLE IF NOT EXISTS `proyects` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `logo_url` varchar(255) NOT NULL,
  `title_proyect` varchar(255) NOT NULL,
  `descrip_proyect` varchar(255) NOT NULL,
  `title_button` varchar(255) NOT NULL,
  `url_proyect` varchar(255) NOT NULL,
  `background_color_proyect` varchar(255) NOT NULL,
  `screen_url_proyect` varchar(255) NOT NULL,
  `tags_proyect` varchar(255) NOT NULL,
  `type_skin` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;

--
-- Volcado de datos para la tabla `proyects`
--

INSERT INTO `proyects` (`id`, `logo_url`, `title_proyect`, `descrip_proyect`, `title_button`, `url_proyect`, `background_color_proyect`, `screen_url_proyect`, `tags_proyect`, `type_skin`, `created_at`, `updated_at`) VALUES
(1, '/dinersclub/dinersclub-logo.png', 'Sitio publico y privado', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'VER DETALLES', 'https://www.mundodinersclub.com/', '#033886', '/dinersclub/dinersclub.jpg', 'usabilidad,ui,ux,test con usuario', '1', NULL, NULL),
(2, '/derco/logo_derco.png', 'Sitio web 2020', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'VER DETALLES', 'https://derco.com.co/', '#eb2838', '/derco/derco.jpg', 'responsive,ui,ux', '2', NULL, NULL),
(3, '/copec/copec-logo.png', 'TV App', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'VER DETALLES', 'https://ww2.copec.cl/', '#fff', '/copec/copec.jpg', 'usabilidad, ui, ux, test con usuarios', '3', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
