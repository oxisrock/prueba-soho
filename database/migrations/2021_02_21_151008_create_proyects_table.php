<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyects', function (Blueprint $table) {
            $table->id();
            $table->string('logo_url');
            $table->string('title_proyect');
            $table->string('descrip_proyect');
            $table->string('title_button');
            $table->string('url_proyect');
            $table->string('background_color_proyect');
            $table->string('screen_url_proyect');
            $table->string('tags_proyect');
            $table->string('type_skin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyects');
    }
}
