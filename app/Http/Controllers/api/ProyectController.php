<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Proyect;

class ProyectController extends Controller
{
    //
    public function list(){

        try {
            $data = Proyect::get();

            $response['data'] = $data;
            $response['succes'] = true;
            return response()->json($response,200);
        } catch (\Exception $e) {
            response()->json($e->getMessage(),404);
        }
    }
}
