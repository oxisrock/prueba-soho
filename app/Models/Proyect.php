<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyect extends Model
{
    use HasFactory;
    protected $table = "proyects";

    protected $fillable = [
        'logo_url',
        'title_proyect',
        'descrip_proyect',
        'title_button',
        'url_proyect',
        'background_color_proyect',
        'screen_url_proyect',
        'tags_proyect'
    ];

}
