const baseUrl = "http://localhost:8000/api/proyects";
import axios from "axios";
const proyects = {};

proyects.listProyects = async () => {
    const urlList = baseUrl+"/list"
    const res = await axios.get(urlList)
        .then(response=> {return response.data })
        .catch(error=>{ return error; })
    return res;
}

export default proyects
