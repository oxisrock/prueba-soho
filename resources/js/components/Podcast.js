import React, { Component } from 'react';
import { Link } from "react-router-dom";
export default class Podcast extends Component {
    render() {
        return (
            <section className="podcast">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12 col-sm-12 col-lg-8 col-xl-8">
                            <h2><i className="fa fa-podcast" aria-hidden="true"></i> <strong>EN DIRECTO</strong> - SOHO está presente en el <strong>DIGITALBANK MONTEVIDEO</strong>. "Experiencia de usuario" por Alvaro Añón (SEO de Soho).</h2>
                        </div>
                        <div className="col-md-12 col-sm-12 col-lg-4 col-xl-4">
                            <div className="contain-buttons">
                                <a href='#' className="btn-green">
                                    VER EVENTO
                                </a>
                                <a href='#' className="btn-green-light">
                                    PRÓXIMOS EVENTOS
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
