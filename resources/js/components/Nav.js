import React, { Component } from 'react';
import { Link } from "react-router-dom";
export default class Nav extends Component {
    render() {
        return (
            <header className="header">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#">
                            <img src="/img/logo-soho.png" className="d-inline-block align-top" alt=""/>
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <i className="fas fa-bars"></i>
                        </button>

                        <div className="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                            <ul className="navbar-nav ">
                                <li className="nav-item active">
                                    <a className="nav-link" href="#">inicio</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">nosotros</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">servicios</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">casos de éxito</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">únete al equipo</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">contacto</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="hero">
                    <div className="hero-content">
                        <h1 className="text-white">Nos especializamos en <p><strong className="text-green">interfaces digitales que los usuarios aman</strong></p></h1>

                        <a className="btn-green">HABLEMOS DE TU PROYECTO</a>
                    </div>
                </div>
            </header>
        )
    }
}
