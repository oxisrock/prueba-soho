import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import proyectsServices from "../services/Proyects"

function Proyects() {
    const [ listProyects, setListProyects ] = useState([]);

    useEffect(()=>{

        async function fetchDataProyects(){
            const res = await proyectsServices.listProyects();
            console.log(res.data);
            setListProyects(res.data)
        }

        fetchDataProyects();

    },[])
        return (
            <section className="proyects-container">
                <div className="about">
                    <div className="container">
                        <div className="row">
                            <div className="about-single">
                                <div className="col-md-12">
                                    <h2>Proyectos desatacados</h2>
                                    <hr/>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    listProyects.map((item)=>{
                        if (item.type_skin == 1) {
                            return(

                                <div className="proyect-item" style={{ background: item.background_color_proyect ? item.background_color_proyect : "#fff" }}>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-md-12 col-lg-6">
                                                <img className="screen img-fluid" src={"../../img/proyects"+item.screen_url_proyect}/>
                                            </div>
                                            <div className="col-md-12 col-lg-6">
                                                <div className="content-proyect">
                                                    <div className="child-proyect">
                                                        <img className="logo img-fluid" src={"../../img/proyects"+item.logo_url}/>
                                                        <hr/>
                                                        <h2>{item.title_proyect}</h2>
                                                        <p>{item.descrip_proyect}</p>
                                                        <p><i className="fas fa-tags"></i> {item.tags_proyect}</p>
                                                        <a href={item.url_proyect} className="button-proyect" style={{ color: item.background_color_proyect ? item.background_color_proyect : "#fff" }}>{item.title_button}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            )
                        }
                        if (item.type_skin == 2) {
                            return(

                                <div className="proyect-item-two" style={{ background: item.background_color_proyect ? item.background_color_proyect : "#fff" }}>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-md-12 col-lg-6">
                                                <div className="content-proyect">
                                                    <div className="child-proyect">
                                                        <img className="logo img-fluid" src={"../../img/proyects"+item.logo_url}/>
                                                        <hr/>
                                                        <h2>{item.title_proyect}</h2>
                                                        <p>{item.descrip_proyect}</p>
                                                        <p><i className="fas fa-tags"></i> {item.tags_proyect}</p>
                                                        <a href={item.url_proyect} className="button-proyect" style={{ color: item.background_color_proyect ? item.background_color_proyect : "#fff" }}>{item.title_button}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-lg-6">
                                                <img className="screen img-fluid" src={"../../img/proyects"+item.screen_url_proyect}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            )
                        }
                        if (item.type_skin == 3) {
                            return(
                                <div className="proyect-item type-3" style={{ background: item.background_color_proyect ? item.background_color_proyect : "#fff" }}>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-md-12 col-lg-6">
                                                <img className="screen img-fluid" src={"../../img/proyects"+item.screen_url_proyect}/>
                                            </div>
                                            <div className="col-md-12 col-lg-6">
                                                <div className="content-proyect">
                                                    <div className="child-proyect">
                                                        <img className="logo img-fluid" src={"../../img/proyects"+item.logo_url}/>
                                                        <hr/>
                                                        <h2>{item.title_proyect}</h2>
                                                        <p>{item.descrip_proyect}</p>
                                                        <p><i className="fas fa-tags"></i> {item.tags_proyect}</p>
                                                        <a href={item.url_proyect} className="button-proyect">{item.title_button}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            )
                        }
                    })
                }
            </section>
        )
}

export default Proyects;

