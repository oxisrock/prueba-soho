import React, { Component } from 'react';
import { Link } from "react-router-dom";
export default class Footer extends Component {
    render() {
        return (
            <footer className="footer">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-4 col-md-12 d-none d-lg-block">
                            <div className="left-footer">
                                <span><strong><i className="fas fa-copyright"></i> 2017</strong> SOHO internet + humana</span>
                            </div>
                        </div>
                        <div className="col-md-12 col-lg-4">
                            <div className="center-footer">
                                <ul>
                                    <li><a>Visítanos</a></li>
                                    <li><a>Escríbenos</a></li>
                                    <li><a>Llámanos</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-12 col-lg-4">
                            <div className="right-footer">
                                <ul>
                                    <li><a className="face">Facebook</a></li>
                                    <li><a className="twitter">Twitter</a></li>
                                    <li><a className="linkedin">Linkedin</a></li>
                                    <li><a className="youtube">Youtube</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-12 d-block d-lg-none">
                            <div className="left-footer">
                                <span><strong><i className="fas fa-copyright"></i> 2017</strong> SOHO internet + humana</span>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}
