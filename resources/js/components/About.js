import React, { Component } from 'react';
import { Link } from "react-router-dom";
export default class About extends Component {
    render() {
        return (
            <section className="about-container">
                <div className="about">
                    <div className="container">
                        <div className="row">
                            <div className="about-single">
                                <div className="col-md-12">
                                    <h2>17 años de experiencia en pos de tu proyecto</h2>
                                    <hr/>
                                    <p>Especilizados desde 1996 en usabilidad, experiencia del usuario (UX) y diseño de experiencias de proyectos digitales. Aportamos estrategia e innovacion centrada en el usuario y los objetivos de negocio de tu proyecto. Cotrabajando mejoraremos tu tasa de conversion, KPI´S, ROI y los resultados de tu actual publicidad PPC(pay per click)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-4 item">
                            <div className="item-about">
                                <h2>Estrategia, usabilidad & ux</h2>
                                <p>Te ofrecemos excelente usabilidad y experiencia del usuario en tu proyecto, junto a una visión innovadora</p>
                            </div>
                        </div>
                        <div className="col-md-4 item2">
                            <div className="item-about">
                                <h2>Mejoramos la experiencia</h2>
                                <p>Tangiblizamos tu proyecto digital a través de un diseño centrado en el usuario y tecnicas avanzadas</p>
                            </div>
                        </div>
                        <div className="col-md-4 item3">
                            <div className="item-about">
                                <h2>Medición continua de objetivos</h2>
                                <p>Implementación, testing con usuarios y medición continua son necesarias para garantizar el éxito de tu proyecto </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
