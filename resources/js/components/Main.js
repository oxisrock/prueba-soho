import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Header from "./Header"
import Podcast from "./Podcast";
import About from "./About";
import Footer from "./Footer";
import Proyects from "./Proyects";


function Main(){
    return (
        <main>
            <Header/>
            <Podcast/>
            <About/>
            <Proyects/>
            <Footer/>
        </main>
    )
}

export default Main;
// for <div id="app"></div>
ReactDOM.render(<Main />, document.getElementById('app'));
